package org.brienze.fourbar.view;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.EmptyBorder;

import org.brienze.fourbar.entity.Point;
import org.brienze.fourbar.service.GenerateCoordinatesFouBar;
import org.brienze.fourbar.service.ValidateFourBar;
import org.springframework.beans.factory.annotation.Autowired;

public class IndexView extends JFrame {

	private static final long serialVersionUID = 8642071412753931125L;

	private JFrame frame;
	private JPanel contentPane;
	private JButton btnGenerateCoordinates;
	private JComboBox<String> comboBoxTiposMecanismo;
	private JLabel lblTipoMecanismo;
	private JTextField textLinkFixo;
	private JLabel lblLinkFixo;
	private JLabel lblLinkMotor;
	private JLabel lblLinkAcoplador;
	private JLabel lblLinkSeguidor;
	private JTextField textLinkMotor;
	private JTextField textLinkAcoplador;
	private JTextField textLinkSeguidor;
	private JTextField textCoordX;
	private JButton btnValidate;
	private JLabel lblCoordenadaDaOrigem;
	private JLabel lblInputDosValores;
	private String[] tiposMapeadosList = { "Dupla Manivela", "Manivela-Balancim", "Duplo Balancim", "Mecanismo de Ponto Variavel", "Triplo Balancim (Não Grashof)" };
	
	@Autowired
	private ValidateFourBar validateFourBar;
	
	@Autowired
	private GenerateCoordinatesFouBar generateCoordinatesFouBar;
	private JLabel lblCoordX;
	private JLabel lblCoordY;
	private JTextField textCoordY;

	public IndexView() {
		frame = new JFrame();
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e1) {
			e1.printStackTrace();
		}

	}

	/**
	 * @wbp.parser.entryPoint
	 */
	public void show() {
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setBounds(100, 100, 380, 330);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		frame.setContentPane(contentPane);
		contentPane.setLayout(null);

		btnGenerateCoordinates = new JButton("Generate Coordinates");
		btnGenerateCoordinates.setBounds(174, 251, 139, 23);
		contentPane.add(btnGenerateCoordinates);

		btnValidate = new JButton("Validate");
		btnValidate.setBounds(25, 251, 139, 23);
		contentPane.add(btnValidate);

		lblTipoMecanismo = new JLabel("Tipo de mecanismo:");
		lblTipoMecanismo.setBounds(10, 11, 118, 14);
		contentPane.add(lblTipoMecanismo);

		comboBoxTiposMecanismo = new JComboBox<String>();
		comboBoxTiposMecanismo.setModel(new DefaultComboBoxModel<String>(tiposMapeadosList));
		comboBoxTiposMecanismo.setBounds(138, 7, 194, 18);
		contentPane.add(comboBoxTiposMecanismo);

		lblInputDosValores = new JLabel("Input dos valores (cm)");
		lblInputDosValores.setBounds(10, 36, 118, 14);
		contentPane.add(lblInputDosValores);

		lblLinkFixo = new JLabel("Link Fixo:");
		lblLinkFixo.setBounds(10, 61, 48, 14);
		contentPane.add(lblLinkFixo);

		textLinkFixo = new JTextField();
		textLinkFixo.setBounds(138, 58, 96, 20);
		contentPane.add(textLinkFixo);
		textLinkFixo.setColumns(10);

		lblLinkMotor = new JLabel("Link Motor:");
		lblLinkMotor.setBounds(10, 86, 66, 14);
		contentPane.add(lblLinkMotor);

		lblLinkAcoplador = new JLabel("Link Acoplador:");
		lblLinkAcoplador.setBounds(10, 111, 80, 14);
		contentPane.add(lblLinkAcoplador);

		lblLinkSeguidor = new JLabel("Link Seguidor:");
		lblLinkSeguidor.setBounds(10, 136, 80, 14);
		contentPane.add(lblLinkSeguidor);

		textLinkMotor = new JTextField();
		textLinkMotor.setColumns(10);
		textLinkMotor.setBounds(138, 83, 96, 20);
		contentPane.add(textLinkMotor);

		textLinkAcoplador = new JTextField();
		textLinkAcoplador.setColumns(10);
		textLinkAcoplador.setBounds(138, 108, 96, 20);
		contentPane.add(textLinkAcoplador);

		textLinkSeguidor = new JTextField();
		textLinkSeguidor.setColumns(10);
		textLinkSeguidor.setBounds(138, 133, 96, 20);
		contentPane.add(textLinkSeguidor);

		lblCoordenadaDaOrigem = new JLabel("Coordenada da origem do link motor:");
		lblCoordenadaDaOrigem.setBounds(10, 170, 207, 14);
		contentPane.add(lblCoordenadaDaOrigem);

		textCoordX = new JTextField();
		textCoordX.setColumns(10);
		textCoordX.setBounds(68, 192, 66, 20);
		contentPane.add(textCoordX);
		
		lblCoordX = new JLabel("Coord X:");
		lblCoordX.setBounds(10, 195, 48, 14);
		contentPane.add(lblCoordX);
		
		lblCoordY = new JLabel("Coord Y:");
		lblCoordY.setBounds(10, 220, 48, 14);
		contentPane.add(lblCoordY);
		
		textCoordY = new JTextField();
		textCoordY.setColumns(10);
		textCoordY.setBounds(68, 217, 66, 20);
		contentPane.add(textCoordY);

		addActions();
		frame.setVisible(true);
	}
	
	private void addActions() {
		//Validate entry data
		btnValidate.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				String tipo = comboBoxTiposMecanismo.getSelectedItem().toString();
				Map<String, Double> variables = new HashMap<>();
				variables.put("linkFixo", Double.valueOf(textLinkFixo.getText()));
				variables.put("linkMotor", Double.valueOf(textLinkMotor.getText()));
				variables.put("linkAcoplador", Double.valueOf(textLinkAcoplador.getText()));
				variables.put("linkSeguidor", Double.valueOf(textLinkSeguidor.getText()));
				
				Map<String, String> response = validateFourBar.validate(tipo, variables);
				if(response.get("success").equals("true")) {
					JOptionPane.showMessageDialog(null, "Mecanismo validado corretamente!");
				} else {
					JOptionPane.showMessageDialog(null, "Mecanismo não validado! Existem erros nos valores informado.\n" + response.get("message"));
				}
			}
		});
		
		//Generate Coordinates
		btnGenerateCoordinates.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				String tipo = comboBoxTiposMecanismo.getSelectedItem().toString();
				
				Point origin = new Point(Double.valueOf(textCoordX.getText()), Double.valueOf(textCoordY.getText()));
				
				Map<String, Double> variables = new HashMap<>();
				variables.put("linkFixo", Double.valueOf(textLinkFixo.getText()));
				variables.put("linkMotor", Double.valueOf(textLinkMotor.getText()));
				variables.put("linkAcoplador", Double.valueOf(textLinkAcoplador.getText()));
				variables.put("linkSeguidor", Double.valueOf(textLinkSeguidor.getText()));

				
				Map<String, String> response = validateFourBar.validate(tipo, variables);
				if(response.get("success").equals("true")) {
					Map<String, String> coordinates = generateCoordinatesFouBar.generate(origin, variables);
					
					StringBuilder message = new StringBuilder();
					message.append("Mecanismo validado corretamente! \nCoordenadas:\n");
					for (Entry<String, String> entry : coordinates.entrySet()) {
						message.append(entry.getKey() + ": " + entry.getValue() + "\n");
					}
					
					JOptionPane.showMessageDialog(null, message.toString());
				} else {
					JOptionPane.showMessageDialog(null, "Mecanismo não validado! Existem erros nos valores informado.\n" + response.get("message"));
				}
			}
		});
		
	}
	
}
