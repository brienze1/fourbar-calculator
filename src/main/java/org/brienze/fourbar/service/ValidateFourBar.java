package org.brienze.fourbar.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Component;

@Component
public class ValidateFourBar {

	private Map<String, List<String>> tiposMapeados;
	Map<String, Double> variables;
	
	@PostConstruct
	private void init() {
		tiposMapeados = new HashMap<>();
		tiposMapeados.put("Dupla Manivela", Arrays.asList(new String[] { "linkFixo", "<" }));
		tiposMapeados.put("Manivela-Balancim", Arrays.asList(new String[] { "linkMotor&linkseguidor", "<" }));
		tiposMapeados.put("Duplo Balancim", Arrays.asList(new String[] { "linkAcoplador", "<" }));
		tiposMapeados.put("Mecanismo de Ponto Variavel", Arrays.asList(new String[] { "", "=" }));
		tiposMapeados.put("Triplo Balancim (Não Grashof)", Arrays.asList(new String[] { "", ">" }));
	}
	
	public Map<String, String> validate(String tipo, Map<String, Double> variables) {
		this.variables = variables;
		
		Map<String, String> response = new HashMap<>();
		for (Entry<String, List<String>> entry : tiposMapeados.entrySet()) {
			if(entry.getKey().equals(tipo)) {
				try {
					validateMenorLink(entry.getValue().get(0));
					validadeExpression(entry.getValue().get(1));
					response.put("success", "true");
					response.put("message", "Mecanismo validado com sucesso!");
					continue;
				} catch (RuntimeException e) {
					response.put("success", "false");
					response.put("message", e.getMessage());
					continue;
				}
			}
		}
		
		if(response.get("message").isBlank()) {
			response.put("success", "false");
			response.put("message", "Erro: O tipo do mecanismo nao foi encontrado!");
		}
		return response;
	}

	private void validateMenorLink(String menorLink) {
		if(!menorLink.isBlank() && !menorLink.equals("linkMotor&linkseguidor")) {
			for (Entry<String, Double> variable : variables.entrySet()) {
				if(!variable.getKey().equals(menorLink) && variables.get(menorLink).compareTo(variable.getValue())>=0) {
					throw new RuntimeException("Erro: o valor do link " + menorLink + " é maior que o valor do link " + variable.getKey() + ".");
				}
			}
		} else if(!menorLink.isBlank()) {
			boolean motor = true;
			boolean seguidor = true;
			for (Entry<String, Double> variable : variables.entrySet()) {
				if(motor && !variable.getKey().equals("linkMotor") && variables.get("linkMotor").compareTo(variable.getValue())>=0) {
					motor = false;
				}
				if(seguidor && !variable.getKey().equals("linkSeguidor") && variables.get("linkSeguidor").compareTo(variable.getValue())>=0) {
					seguidor = false;
				}
			}
			if(!motor && !seguidor) {
				throw new RuntimeException("Erro: Nenhum dos links adjacentes ao link fixo é o menor link.");
			}
		}
	}
	
	private void validadeExpression(String exp) {
		List<Double> valores = new ArrayList<>();
		for (Entry<String, Double> entry : variables.entrySet()) {
			valores.add(entry.getValue());
		}
		
		Collections.sort(valores);
		System.out.println(valores);
		Double sl = valores.get(0) + valores.get(3);
		Double pq = valores.get(1) + valores.get(2);
		
		if(exp.equals(">") && sl.compareTo(pq)<=0) {
			throw new RuntimeException("Erro: o valor da soma do menor e maior link (" + sl + ") não é maior que o valor da soma dos links intermediarios (" + pq + ").");
		} else if(exp.equals("<") && sl.compareTo(pq)>=0) {
			throw new RuntimeException("Erro: o valor da soma menor e maior link (" + sl + ") é maior que o valor da soma dos links intermediarios (" + pq + ").");
		} else if(exp.equals("=") && sl.compareTo(pq)!=0) {
			throw new RuntimeException("Erro: o valor da soma menor e maior link (" + sl + ") não é igual ao valor da soma dos links intermediarios (" + pq + ").");
		}
	}
	
}
















