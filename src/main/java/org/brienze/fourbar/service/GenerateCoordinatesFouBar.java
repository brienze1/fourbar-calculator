package org.brienze.fourbar.service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.brienze.fourbar.entity.Point;
import org.springframework.stereotype.Component;

@Component
public class GenerateCoordinatesFouBar {
	
	final double THRESHOLD = .001;

	public Map<String, String> generate(Point origin, Map<String, Double> variables) {
		Map<String, String> coordinates = new HashMap<>();
		coordinates.put("Point 1", origin.toString());
		Point endLinkFixo = new Point(origin.getX() + variables.get("linkFixo"), origin.getY());

		Integer numPoints = 1000;
		while(!coordinates.containsKey("Point 2") && !coordinates.containsKey("Point 3")) {
			List<Point> pointsLinkMotor = generatePoints(origin, variables.get("linkMotor"), numPoints);
			
			List<Point> pointsLinkSeguidor = generatePoints(endLinkFixo, variables.get("linkSeguidor"), numPoints);
			
			for (Point pointMotor : pointsLinkMotor) {
				if(!coordinates.containsKey("Point 2") && !coordinates.containsKey("Point 3")) {
					for (Point pointSeguidor : pointsLinkSeguidor) {
						if (Math.abs(distanceCalculator(pointMotor, pointSeguidor) - variables.get("linkAcoplador")) < THRESHOLD) {
								coordinates.put("Point 2", pointMotor.toString());
								coordinates.put("Point 3", pointSeguidor.toString());
								break;
						}
					}
				} else {
					break;
				}
			}
			
			numPoints *= 10;
		}
		
		coordinates.put("Point 4", endLinkFixo.toString());
		
		return coordinates;
	}

	private List<Point> generatePoints(Point origin, Double radius, Integer numPoints) {
		final Point[] points = new Point[numPoints];
		for (int i = numPoints-1; i >= 0; --i) {
			Double angle = Math.toRadians(((double) i / numPoints) * 360d);
			points[i] = new Point(origin.getX() + (Math.cos(angle) * radius), origin.getY() + Math.sin(angle) * radius);
		}

		return Arrays.asList(points);
	}

	private Double distanceCalculator(Point origin, Point end) {
	    double distance;
	    distance = Math.sqrt((end.getX()-origin.getX())*(end.getX()-origin.getX()) + (end.getY()-origin.getY())*(end.getY()-origin.getY()));	
	    
        System.out.println("distancebetween"+"(" + origin.getX() + "," + origin.getY() + ")," + "(" + end.getX() + "," + end.getY() + ")===>" + distance);
        
        return distance;
	}
	
}
