package org.brienze.fourbar;

import org.brienze.fourbar.view.IndexView;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class App implements CommandLineRunner {
	
    public static void main( String[] args ) {
    	new SpringApplicationBuilder(App.class).headless(false).run(args);
    }

	@Bean
	public IndexView showInitialScreen() {
		return new IndexView();
	}

	public void run(String... args) throws Exception {
		showInitialScreen().show();		
	}
	
}
